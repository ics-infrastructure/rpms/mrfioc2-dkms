%define module_name mrfioc2
%define version 2.2.1rc1
%define module_url https://github.com/icshwi/%{module_name}

Name:           %{module_name}-dkms
Version:        %{version}
Release:        1
Vendor:         ESS
Summary:        DKMS-ready kernel module - mrfioc2 driver
License:        EPICS Open license
URL:            %{module_url}
Source:         %{module_url}/archive/%{version}.tar.gz
BuildRequires:  autoconf libtool gcc gcc-c++
Requires(pre):  dkms
Requires(post): dkms
Buildroot:      %{_tmppath}/%{name}-%{version}-root

%description
%(cat %{_topdir}/README.md)

%prep
%setup -qn %{module_name}-%{version}

%build

%clean
rm -fr $RPM_BUILD_ROOT

%install
# Copy all sources required for dkms to build the module
mkdir -p %{buildroot}/usr/src/%{module_name}-%{version}-%{release}
cp -a mrmShared/linux/* %{buildroot}/usr/src/%{module_name}-%{version}-%{release}/

# Create udev rule to give normal users read/write access
mkdir -p ${RPM_BUILD_ROOT}/etc/udev/rules.d
echo 'KERNEL=="uio*", ATTR{name}=="mrf-pci", MODE="0666"' > ${RPM_BUILD_ROOT}/etc/udev/rules.d/99-mrfioc2.rules

# Create dkms.conf file
cat > %{buildroot}/usr/src/%{module_name}-%{version}-%{release}/dkms.conf <<EOF

PACKAGE_NAME="%{module_name}"
PACKAGE_VERSION="%{version}-%{release}"

MAKE[0]="make -C \${kernel_source_dir} SUBDIRS=\${dkms_tree}/\${PACKAGE_NAME}/\${PACKAGE_VERSION}/build modules"
CLEAN="make -C \${kernel_source_dir} SUBDIRS=\${dkms_tree}/\${PACKAGE_NAME}/\${PACKAGE_VERSION}/build clean"

BUILT_MODULE_NAME[0]="mrf"
DEST_MODULE_LOCATION[0]="/kernel/drivers/%{module_name}"

AUTOINSTALL="yes"
REMAKE_INITRD="no"
EOF

%post
%udev_rules_update
dkms add -m %{module_name} -v %{version}-%{release} --rpm_safe_upgrade
dkms build -m %{module_name} -v %{version}-%{release} --rpm_safe_upgrade
dkms install -m %{module_name} -v %{version}-%{release} --rpm_safe_upgrade

%preun
dkms remove -m %{module_name} -v %{version}-%{release} --rpm_safe_upgrade --all ||:

%postun
%udev_rules_update

%files
%defattr(-,root,root)
/usr/src/%{module_name}-%{version}-%{release}
/etc/udev/rules.d/99-mrfioc2.rules

%changelog
* Mon Apr 27 2020 Jerzy Jamroz <jerzyjamroz@ess.eu>
- Refer to the module URL CHANGELOG.md