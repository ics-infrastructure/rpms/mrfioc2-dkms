# mrfioc2-dkms

CentOS RPM for [mrfioc2](http://epics.sourceforge.net/mrfioc2/) kernel module: driver for Micro Research Finland event timing system devices

This RPM includes:

- the source for the kernel module managed by dkms (mrf)
- udev rule to give read/write access to all users

To build a new version of the RPM:

- update the version in the spec file
- push your changes to check that the RPM can be built by gitlab-ci
- tag the repository and push the tag for the RPM to be uploaded to artifactory rpm-ics repo
